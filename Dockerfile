FROM openjdk:11
ARG JAR_FILE=build/libs/service-registry-0.0.1.jar
COPY ${JAR_FILE} service-registry.jar
ENTRYPOINT ["java","-jar","/service-registry.jar"]